# README #

* Use steganography to hide messages in images!
* Part of an old undergrad graphics assignment

### How To Use ###

To encode an image file 'image' with a message defined in text file 'message'

```
java Driver encode image message
```

To decode an image file 'image'

```
java Driver decode image
```

### Encryption Method ###

The program breaks the text up into a binary string representing each character as an integer.
It then feeds each of the bits into the provided image, replacing the least significant bit of each pixel color value.
This way each pixel can hold up to 3 bits of information without causing any notable distortion.