import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.System;
import java.util.Scanner;

import javax.imageio.ImageIO;

public class Driver
{
    public static void main(String[] args)
    {
		BufferedImage img = null;
	
		if(args.length > 1)
			try {
				img = ImageIO.read(new File(args[1]));
	    
				if(args[0].equals("decode"))
					SecretKeeper.decode(img);
		
				else if(args.length == 3 && args[0].equals("encode"))
				{
					try {
						Scanner in = new Scanner(new File(args[2]));
						String ext = args[1].substring(args[1].indexOf('.') + 1);
						System.out.println(ext);
						String text = "";
						File out = new File("out." + ext);
		    
						while(in.hasNext())
							text += in.nextLine() + "\n";
			
						img = SecretKeeper.encode(text, img);
			
						ImageIO.write(img, ext, out);
			
						in.close();
						} catch(FileNotFoundException e) {
							System.out.println("Failed to load text file");
						} // catch
				} // else if
				else
					System.out.println("Args: 'decode file.image', 'encode file.image file.text'");
			} catch(IOException e) {
				System.out.println("Failed to load image file");
			} // catch
		else
			System.out.println("Args: 'decode file.image', 'encode file.image file.text'");
    } // main
} // Driver