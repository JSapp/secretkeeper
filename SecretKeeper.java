import java.awt.image.BufferedImage;
import java.lang.System;
import java.util.Arrays;

/**
 * Tryin' out some static stuff
 */
public final class SecretKeeper
{
    private SecretKeeper()
    {
    } // SecretKeeper

    /**
     * Encodes message in image
     *
     * @param text Message
     * @param image Image
     *
     * @return Encoded image
     */
    public static BufferedImage encode(String text, BufferedImage image)
    {
		long startTime = System.currentTimeMillis();
		int k = 0;

		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_3BYTE_BGR);

		for(int i = 0; i < image.getWidth(); i++)
			for(int j = 0; j < image.getHeight(); j++)
				newImage.setRGB(i, j, image.getRGB(i, j));

		text += "\0";

		// I loop backwards because my code is spaghetti or something
		int[] code = new int[text.length() * 7];
		int goodNumberANumberIdLikeToMeet = 0; // I'm sure there's a better way to do this but my mind went on vacation and took the keys to the house
		for(int i = 0; i < text.length(); i++)
			for(int j = 6; j > -1 ; j--)
				code[goodNumberANumberIdLikeToMeet++] = ((int)text.charAt(i) >> j) & 1;

		// Do the magic (In this case the magic looks kind of messy but it's still reasonably fast so who cares!!!)
		for(int i = 0; i < newImage.getWidth(); i++)
		{
			for(int j = 0; j < newImage.getHeight(); j++)
			{
				int n = newImage.getRGB(i, j);

				if(code[k++] == 0)
					n &= ~1;
				else
					n |= 1;
				
				if(k == code.length)
				{
					i = newImage.getWidth();
					break;
				} // if
				
				if(code[k++] == 0)
					n &= ~(1 << 8);
				else
					n |= 1 << 8;
				
				if(k == code.length)
				{
					i = newImage.getWidth();
					break;
				} // if
				if(code[k++] == 0)
					n &= ~(1 << 16);
				else
					n |= 1 << 16;
				if(k == code.length)
				{
					i = newImage.getWidth();
				break;
				} // if

				newImage.setRGB(i, j, n);
			} // for
		} // for

		long endTime = System.currentTimeMillis();
	
		System.out.println("\nTime taken: " + (endTime - startTime) + "ms\n");

		return newImage;
    } // encode

    /**
     * Decodes message in image
     *
     * @param image Image
     */
    public static void decode(BufferedImage image)
    {
		/*
		 * The repetition in the critical loop really bothers me but pixels are returned as 32 bit integers, so I'm not sure how to avoid it right now
		 * Could use a 3 iteration loop in the tertiary layer, but the computations would be the same
		 */
		int[] code = new int[image.getWidth() * image.getHeight()];
		int bit = 0;
		int k = 6, l = 0; // k helps place bits, l is the iterator for code array

		for(int i = 0; i < image.getWidth(); i++)
		{
			for(int j = 0; j < image.getHeight(); j++)
			{	
				int n = image.getRGB(i, j);

				bit |= (n & 1) << k--;
				
				if(k < 0)
				{
					k = 6;
					code[l++] = bit;
					System.out.print((char)bit);
					if(bit == 0 || bit == 1)
					System.exit(0);
					else
					bit = 0;
				} // if
				
				bit |= ((n >> 8) & 1) << k--;
				
				if(k < 0)
				{
					k = 6;
					code[l++] = bit;
					System.out.print((char)bit);
					if(bit == 0 || bit == 1)
					System.exit(0);
					else
					bit = 0;
				} // if
				
				bit |= ((n >> 16) & 1) << k--;
				
				if(k < 0)
				{
					k = 6;
					code[l++] = bit;
					System.out.print((char)bit);
					if(bit == 0 || bit == 1)
					System.exit(0);  
					else
					bit = 0;
				} // if
			} // for
		} // for    
    } // decode
    
    /**
     * Adjust rgb bitcode for color model
     * Unused
     *
     * @param r Red bit
     * @param g Green bit
     * @param b Blue bit
     *
     * @return Converted int
     */
    private static int shift(int r, int g, int b)
    {
		return (r << 16) | (g << 8) | b;
    } // shift

    /**
     * Does some nonsense
     *
     * @param c Color as base 10 integer
     * @param r Red bit
     * @param g Green bit
     * @param b Blue bit
     *
     * @return Color model int
     */
    private static int codePixel(int c, int r, int g, int b)
    {
		//System.out.println("COLORS: " + r + " " + g + " " + b);
		// This could probably be optimized
		if(r == 0)
			c &= 0;
		else
			c |= 1;
		if(g == 0)
			c &= ~(1 << 8);
		else
			c |= 1 << 8;
		if(b == 0)
			c &= ~(1 << 16);
		else
			c |= 1 << 16;

		return c;
    } // codePixel
} // SecretKeeper